package com.rabotauasearch.framework.pages;

import com.rabotauasearch.framework.core.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class VacancyHomePage extends BasePage {

    @FindBy(id = "beforeContentZone_vcVwPopup_plhAdditionalButtons")
    public WebElement moreButton;

    @FindBy(id = "beforeContentZone_vcVwPopup_VacancySendToFriendLink")
    public WebElement sendByMailButton;

    @FindBy(id = "sendToFriend_txtTo\" class=\"input-block-level txtToemail")
    public WebElement textfieldSendTo;

    @FindBy(id = "beforeContentZone_vcVwPopup_sent")
    public WebElement sentIndicator;

    @FindBy(id = "beforeContentZone_vcVwPopup_linkApply")
    public WebElement sendResumeButton;

    @FindBy(id = "beforeContentZone_vcVwPopup_vacancyApplyForm_txFromEMail")
    public WebElement emailTextFieldApplyForm;

    @FindBy(id = "beforeContentZone_vcVwPopup_vacancyApplyForm_txtSurName")
    public WebElement surNameTextFieldApplyForm;

    @FindBy(id = "beforeContentZone_vcVwPopup_vacancyApplyForm_txtName")
    public WebElement nameTextFieldApplyForm;

    //Vacancy description elements
    @FindBy(css = ".VacancyTitle")
    public WebElement vacancyTitle;

    public void apllyIfVacancyHeaderCorrectly(String expectedVacancyHeader){
        if(vacancyTitle.getText().contains(expectedVacancyHeader)){
            applyToVacancyByResumePDF();
        }
        else{
            System.out.println("Вакансия не соответствует поиску!!!");
        }
    }

    public void applyToVacancyByResumePDF(){
        if(!(getWebDriver().findElements(By.id("beforeContentZone_vcVwPopup_sent")).isEmpty())){
            System.out.println("Вы уже откликались");
        }
        else{
            sendResumeButton.click();
        }
    }

    public void chooseMoreVacancyOptions(){
        hover(moreButton);
    }

    public void sendVacancyByMail(String mail){
        clickOnElement(sendByMailButton);
        inputTextAndPressEnter(textfieldSendTo, mail);
    }

    public void enterEmailApplyForm(String emailText){
        inputText(emailTextFieldApplyForm, emailText);
    }

    public void enterSurNameApplyForm(String surName){
        inputText(surNameTextFieldApplyForm, surName);
    }

    public void enterNameApplyForm(String surName){
        inputText(nameTextFieldApplyForm, surName);
    }


    public VacancyHomePage(WebDriver driver) {
        super(driver);
    }
}
