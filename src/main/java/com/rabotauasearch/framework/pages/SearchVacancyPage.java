package com.rabotauasearch.framework.pages;

import com.rabotauasearch.framework.core.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

import static com.rabotauasearch.framework.core.CustomConditions.elementHasText;

public class SearchVacancyPage extends BasePage{

    @FindBy(id = "beforeContentZone_HorizontalContainer1_Keyword")
    public WebElement searchField;

    @FindBy(id = "beforeContentZone_HorizontalContainer1_lnkSearch")
    public WebElement findVacancyButton;

    @FindBy(id = "beforeContentZone_HorizontalContainer2_Keyword")
    public WebElement searchByHeadingField;

    @FindBy(id = "beforeContentZone_HorizontalContainer1_CityPickerWork_lblCityName")
    public WebElement regionDropDownList;

    @FindBy(css = ".vv .v:nth-child(3) .tags")
    public WebElement firstByVacancySearchResult;

    @FindBy(css = ".vv .v:nth-child(3) .rua-g-clearfix")
    public WebElement firstByVacancySearchResultSubHeader;

    // Search results
    @FindBy(css = ".vv .v:nth-child(1) .rua-g-clearfix .t")
    public WebElement firstByVacancySearchResultHeader;

    @FindBy(css = ".vv .v:nth-child(2) .rua-g-clearfix .t")
    public WebElement secondByVacancySearchResultHeader;

    @FindBy(css = ".vv .v:nth-child(2) .rua-g-clearfix .t")
    public WebElement thirdByVacancySearchResultHeader;

    public void searchVacancyByName(String vacancyName){
        simpleSearch(searchField, vacancyName);
    }

    public void searchVacancyByHeading(String vacancyname){
        simpleSearch(searchByHeadingField, vacancyname);
    }

    public void clearSearchField(){
        searchField.clear();
    }

    public void findVacancy(){
        clickOnElement(findVacancyButton);
    }

    public void assertThatFirstSearchResultInRegion(String cityText){
        assertThat(elementHasText(firstByVacancySearchResultSubHeader, cityText));
    }

    public void assertThatFirstSearchResultHasTag(String tagText){
        assertThat(elementHasText(firstByVacancySearchResult, tagText));
    }

    public void sortSearchResultByRegion(int index){
        regionDropDownList.click();
        List <WebElement> menus = getWebDriver().findElements(By.cssSelector("#beforeContentZone_HorizontalContainer1_CityPickerWork_topLevelCities>li>a"));
        WebElement desiredMenu = menus.get(index);
        desiredMenu.click();
        }

    public void sortSearchResultByRegionKyiv(){
        sortSearchResultByRegion(5);
    }

    public void openFirstVacancySearchResult(){
        clickOnElement(firstByVacancySearchResultHeader);
    }

    public void openSecondVacancySearchResult(){
        clickOnElement(secondByVacancySearchResultHeader);
    }

    public void openThirdVacancySearchResult(){
        clickOnElement(thirdByVacancySearchResultHeader);
    }

    public SearchVacancyPage(WebDriver driver){
        super(driver);
    }
}
