package com.rabotauasearch.framework.pages;

import com.rabotauasearch.framework.core.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;

public class LoginPage extends BasePage {

    @FindBy(id = "centerZone_ZoneLogin_btnLogin")
    public WebElement signInButton;

    @Step
    public void signIn(){
        putCorrectLoginData();
        signInButton.click();
    }

    public LoginPage(WebDriver driver) {
        super(driver);
    }
}
