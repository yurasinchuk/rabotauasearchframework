package com.rabotauasearch.framework.pages;

import com.rabotauasearch.framework.core.BasePage;
import com.rabotauasearch.framework.core.CustomConditions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;

public class RabotauaPage extends BasePage {

    // Header menu elements Anon user
    @FindBy(css = ".header-anon .header-inner .header-item:nth-child(4) .header-link")
    public WebElement loginHeaderAnon;

    // Header menu elements
    @FindBy(css = ".header-inner .header-item:nth-child(1) .ri_logo")
    public WebElement logoHeader;

    @FindBy(css = ".header-inner .header-item:nth-child(2) .header-link")
    public WebElement findVacancyHeader;

    @FindBy(css = ".header-inner .header-item:nth-child(2) .header-menu-inner .header-menu-item:nth-child(1) .header-menu-link")
    public WebElement findVacancyByHeader;

    @FindBy(css = ".header-inner .header-item:nth-child(2) .header-menu-inner .header-menu-item:nth-child(2) .header-menu-link")
    public WebElement findVacancyByProfessions;

    @FindBy(css = ".header-inner .header-item:nth-child(2) .header-menu-inner .header-menu-item:nth-child(3) .header-menu-link")
    public WebElement findVacancyByCity;

    @FindBy(css = ".header-inner .header-item:nth-child(2) .header-menu-inner .header-menu-item:nth-child(4) .header-menu-link")
    public WebElement findVacancyByCompany;

    @FindBy(css = ".header-inner .header-item:nth-child(3) .header-link")
    public WebElement recomendationsHeader;

    @FindBy(css = ".header-inner .header-item:nth-child(4) .header-link")
    public WebElement interestingHeader;

    @FindBy(css = ".header-inner .header-item:nth-child(5) .header-link")
    public WebElement applicationsHistoryHeader;

    @FindBy(css = ".header-inner .header-item:nth-child(7) .header-link")
    public WebElement myResumeHeader;

    @FindBy(css = ".header-inner .header-item:nth-child(10) .header-link")
    public WebElement settings;

    @FindBy(id = "Header_JobsearcherLoggedin_btnExit")
    public WebElement exitButton;

    // Other elements

    @FindBy(id = "beforeContentZone_vacSearch_Keyword")
    public WebElement searchSimpleField;

    // My Resume basePage elements
    @FindBy(id = "centerZone_ResumeList_grVwResume_lnkRefresh_0")
    public WebElement updateResumeButton;

    // Search Job basePage elements

    @FindBy(id = "beforeContentZone_HorizontalContainer4_Keyword")
    public WebElement searchByProfessionsField;

    // Header menu actions
    @Step
    public void given(){
        logoHeader.click();
    }

    @Step
    public void openSignIn(){
        clickOnElement(loginHeaderAnon);
    }

    public void openFindVacancy(){
        clickOnElement(findVacancyHeader);
    }

    public void openFindVacancyByCompany(){
        clickOnElement(findVacancyByCompany);
    }

    @Step
    public void openMyResume(){
        clickOnElement(myResumeHeader);
    }

    public void simpleSearchVacancy(String vacancyName){
        simpleSearch(searchSimpleField, vacancyName);
    }

    public void hoverFindJob(){
        hover(findVacancyHeader);
    }

    @Step
    public void hoverSettings(){
        hover(settings);
    }

    @Step
    public void exit(){
        clickOnElement(exitButton);
    }

    public RabotauaPage(WebDriver driver){
        super(driver);
    }

    // Conditions
    @Step
    public void assertThatUserLogged (){
        hoverSettings();
        assertThat(CustomConditions.elementHasText(exitButton, "Выйти"));
    }
}