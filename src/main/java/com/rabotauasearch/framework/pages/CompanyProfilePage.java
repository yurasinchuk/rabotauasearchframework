package com.rabotauasearch.framework.pages;

import com.rabotauasearch.framework.core.BasePage;
import com.rabotauasearch.framework.core.CustomConditions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CompanyProfilePage extends BasePage {

    @FindBy(css = "#prf-header>table>tbody>tr>td>h2")
    public WebElement companyNameProfile;

    @FindBy(id ="centerZone_centerZone_CompanyInfoCenterAreaSwitcher1_ctl00_menuTop_rptrCustomMenu_hprLnkMenuItem_0")
    public WebElement openedVacanciesTab;

    @FindBy(id = "centerZone_centerZone_CompanyInfoCenterAreaSwitcher1_ctl00_ctl00_CompanyListFilter1_ddlCity")
    public WebElement dropdownCompanyOffice;

    @FindBy(id = "centerZone_centerZone_CompanyInfoCenterAreaSwitcher1_ctl00_ctl00_CompanyListFilter1_ddlRubric")
    public WebElement dropdownVacByRubrics;

    @FindBy(xpath = ".//*[@id='centerZone_centerZone_CompanyInfoCenterAreaSwitcher1_ctl00_ctl00_GridView1']/tbody/tr[3]/td/div/div")
    public WebElement selectedCity;

    @FindBy(id = "centerZone_centerZone_CompanyInfoCenterAreaSwitcher1_ctl00_ctl00_GridView1_hprLnkVacancyName_4")
    public WebElement mostNewVac;

    public void assertCompanyName(String companyName){
        assertThat(CustomConditions.elementHasText(companyNameProfile, companyName));
    }

    public void openCompanyVacanciesTab(){
        clickOnElement(openedVacanciesTab);
    }

    public void openMostNewVac(){
        mostNewVac.click();
    }

    public void selectOfficeByCity(String officeIn){
        selectDropdownByText(dropdownCompanyOffice, officeIn);
    }

    public void selectByRubrics(String rubricName){
        selectDropdownByText(dropdownVacByRubrics, rubricName);
    }

    public void assertThatSelectedCity(String selectedCityName){
        assertThat(CustomConditions.elementHasText(selectedCity, selectedCityName));
    }

    public CompanyProfilePage(WebDriver driver) {
        super(driver);
    }
}
