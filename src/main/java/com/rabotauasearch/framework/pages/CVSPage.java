package com.rabotauasearch.framework.pages;

import com.rabotauasearch.framework.core.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;

public class CVSPage extends BasePage {

    @FindBy(id = "centerZone_ResumeList_grVwResume_lnkRefresh_0")
    public WebElement updateFirstResumeButton;

    @FindBy(id = "centerZone_ResumeList_grVwResume_lnkRefresh_1")
    public WebElement updateSecondResumeButton;

    @Step
    public void updateFirstResume(){
        clickOnElement(updateFirstResumeButton);
    }

    @Step
    public void updateSecondResume(){
        clickOnElement(updateSecondResumeButton);
    }

    public CVSPage(WebDriver driver) {
        super(driver);
    }
}
