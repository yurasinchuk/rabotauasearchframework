package com.rabotauasearch.framework.pages;

import com.rabotauasearch.framework.core.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SearchByCompanyPage extends BasePage {

    @FindBy(id = "beforeContentZone_jobSearchVIPCompaniesSearch_txBxCompanyName")
    public WebElement searchByCompanyInputField;

    @FindBy(id = "beforeContentZone_jobSearchVIPCompaniesSearch_lnkBtnSearch")
    public WebElement findCompanyButton;

    @FindBy(css = ".vv .v:nth-child(1) .rua-g-clearfix .t")
    public WebElement firstByCompanySearchResult;


    public void enterCompanyName(String company){
        inputText(searchByCompanyInputField, company);
    }

    public void findCompany(){
        clickOnElement(findCompanyButton);
    }

    public void selectFirstByCompanySearchResult(){
        clickOnElement(firstByCompanySearchResult);
    }

    public SearchByCompanyPage(WebDriver driver) {
        super(driver);
    }
}
