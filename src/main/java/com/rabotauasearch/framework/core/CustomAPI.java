package com.rabotauasearch.framework.core;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import static org.openqa.selenium.support.ui.ExpectedConditions.presenceOfElementLocated;

public abstract class CustomAPI {

    public abstract WebDriver getWebDriver();

    public static final String PATH_TO_PROPERTIES = "src/main/resources/config.properties";

    public void putCorrectLoginData() {

        FileInputStream fileInputStream;

        Properties prop = new Properties();

        try{
            fileInputStream = new FileInputStream(PATH_TO_PROPERTIES);
            prop.load(fileInputStream);

        } catch (IOException e){
            System.out.println("File not found:" + PATH_TO_PROPERTIES);
            e.printStackTrace();
        }

        getWebDriver().findElement(By.id("centerZone_ZoneLogin_txLogin")).sendKeys(prop.getProperty("login"));
        getWebDriver().findElement(By.id("centerZone_ZoneLogin_txPassword")).sendKeys(prop.getProperty("password"));
    }

    // Primitive actions
    public void clickOnElement(WebElement element){
        element.click();
    }

    public void hover(WebElement hoverButton){
        Actions hoverAction = new Actions(getWebDriver());
        WebElement button = hoverButton;
        hoverAction.moveToElement(button).build().perform();
    }

    public void hoverAndClick(WebElement hoverElement, WebElement clickElement){
        hover(hoverElement);
        clickOnElement(clickElement);
    }

    public void waiterElement(String buttonById){
        (new WebDriverWait(getWebDriver(), 5)).until(
                presenceOfElementLocated(By.id(buttonById)));
    }

    public void assertThat(ExpectedCondition<Boolean> condition){
        (new WebDriverWait(getWebDriver(), 5)).until(condition);
    }

    public void simpleSearch(WebElement searchFieldBy, String text){
        searchFieldBy.sendKeys(text, Keys.ENTER);
    }

    public void inputText(WebElement textField, String text){
        textField.clear();
        textField.sendKeys(text);
    }

    public void inputTextAndPressEnter(WebElement textField, String text){
        textField.clear();
        textField.sendKeys(text, Keys.ENTER);
    }

    public void selectDropdownByText(WebElement selectElem, String dropdownMenuText){
        Select select = new Select(selectElem);
        select.selectByVisibleText(dropdownMenuText);
    }

}
