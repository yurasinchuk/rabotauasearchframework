package com.rabotauasearch.framework.core;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class BasePage extends CustomAPI{

    @Override
    public WebDriver getWebDriver(){
        return driver;
    }

    private  WebDriver driver;

    public BasePage(WebDriver driver){
        this.driver=driver;
        PageFactory.initElements(driver, this);
    }

}
