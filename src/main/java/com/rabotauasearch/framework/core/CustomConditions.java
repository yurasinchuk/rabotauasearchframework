package com.rabotauasearch.framework.core;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;

import static org.openqa.selenium.support.ui.ExpectedConditions.textToBePresentInElement;

public class CustomConditions {

    public static ExpectedCondition<Boolean> elementHasText(WebElement element, String expectedText){
        return textToBePresentInElement(element, expectedText);
    }

    public static ExpectedCondition<Boolean> selementIsPresent(WebElement element){
        return selementIsPresent(element);
    }
}
