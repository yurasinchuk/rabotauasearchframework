package com.rabotauasearch.framework.core;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

public class BaseTest extends CustomAPI{

    @Override
    public WebDriver getWebDriver(){
        return driver;
    }

    public static WebDriver driver = new FirefoxDriver();

    @BeforeClass
    public static void openPage(){
        driver.get("http://rabota.ua/");
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }

    @AfterClass
    public static void tearDown(){
        driver.quit();
    }
}
