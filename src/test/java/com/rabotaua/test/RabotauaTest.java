package com.rabotaua.test;

import com.rabotauasearch.framework.core.BaseTest;
import com.rabotauasearch.framework.pages.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

public class RabotauaTest extends BaseTest {

    LoginPage loginPage = new LoginPage(driver);
    RabotauaPage rabotauaPage = new RabotauaPage(driver);
    SearchVacancyPage searchPage = new SearchVacancyPage(driver);
    VacancyHomePage vacancyHomePage = new VacancyHomePage(driver);
    CVSPage cvsPage = new CVSPage(driver);

    @Before
    public void loginUser(){
        rabotauaPage.openSignIn();
        loginPage.signIn();
        rabotauaPage.assertThatUserLogged();
        rabotauaPage.given();
    }

    @Test
    @Ignore(" waiting to review ")
    public void searchVacancyOnMainPageTest(){
        rabotauaPage.simpleSearchVacancy("Junior QA Engineer");
        searchPage.sortSearchResultByRegionKyiv();
        searchPage.findVacancy();
        searchPage.assertThatFirstSearchResultInRegion("Киев");
        searchPage.openFirstVacancySearchResult();
        vacancyHomePage.apllyIfVacancyHeaderCorrectly("QA Engineer");
    }

    @Test
    public void updatePublishedResumeTest(){
        rabotauaPage.openMyResume();
        cvsPage.updateFirstResume();
        cvsPage.updateSecondResume();
    }

    @After
    public void logOut(){
        rabotauaPage.hoverSettings();
        rabotauaPage.waiterElement("Header_JobsearcherLoggedin_btnExit");
        rabotauaPage.exit();
    }
}

