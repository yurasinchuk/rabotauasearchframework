# Rabotaua Test Framework #
Example of test framework for [Rabotaua](http://rabota.ua/).
Used technologies and tools: **[Java](https://www.oracle.com/java/index.html), [Selenium WebDriver](http://www.seleniumhq.org/projects/webdriver/), [JUnit](http://junit.org/), [Allure](http://allure.qatools.ru/), [Maven](https://maven.apache.org/), [Jenkins](http://jenkins-ci.org/)**. 
## Tests ##
### Test Scenarios: Registered User###
```
#!java

// Pre conditions

public void loginUser(){

// open sign in page
// enter email and password
// assert that user logged
// go to main page (given)

}

//@Test

public void updatePublishedResumeTest(){

// go to my resume page
// update first resume
// update second resume

}

//@Test

public void searchVacancyTest(){
	
// open Find Job
// enter “key word” to search field
// sort vacancies by city (Kiev)
// start find job action
// assert that first search result has tag “selected city”
// open More parameters
// by Rubrics: select rubric “IT”
// by Salary: enter “1000”
// deselect checkbox “показывать вакансии без зп”
// deselect checkbox “показывать вакансии агенств”
// by Schedule: select full time “полная занятость”
// by Proflevel: select “специалист”
// deselect checkbox “показывать вакансии агенств”
// start find job action
}

//@Test

public void searchVacancyTest(){
	
// open Find Job
// enter “key word” to search field
// assert that first search result has tag “vacancy name”
// sort vacancies by city
// assert that first search result has tag “selected city”
// open vacancy
// assert that user not sent resume to this vacancy earlier
// apply to vacancy if not sent earlier == true
// assert that users contact information enter correctly
// choose sent resume from site
// add cover letter
// choose correct English level if option present
// option check mail vacancies
// option agreement
// sent resume

}


// Post conditions

public void logOut(){
// hover Settings
// click on Exit btn
}


```